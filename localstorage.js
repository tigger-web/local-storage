/* =======================
   LocalStorage javascript
   ======================= */


/* ---------
   Constants
   --------- */

const STORAGE_KEY = "number"


/* ----------------
   Global variables
   ---------------- */

// objects in page
let storeButton
let clearButton
let numberField
let storedValueOutput
let errorBlock
let infoBlock


/* -----------
   Boot loader
   ----------- */

// wait for document loaded before init components
document.addEventListener('DOMContentLoaded', function() { initComponents() })


/* ---------
   Functions
   --------- */

// Debug method will simply write string param to console
function dbg(info) {
    console.log(info)
}

function showError(errorMsg) {
    errorBlock.textContent = errorMsg
}

function showInfo(msg) {
    infoBlock.textContent = msg
}

function showStoredValue() {
    storedValueOutput.textContent = localStorage.getItem(STORAGE_KEY)
}

function clearInput() {
    numberField.value = ""
}

function storeValue() {
    let rawValue = numberField.value
    if (isNaN(rawValue)) {
        showError(rawValue + " is not a number, cannot store")
        showInfo("")
    } else {
        showError("") // clear any last error
        localStorage.setItem(STORAGE_KEY, parseFloat(rawValue))
        clearInput()
        showStoredValue()
        showInfo("New value stored")
    }
}

function clear() {
    showError("") // clear any last error
    localStorage.removeItem(STORAGE_KEY)
    clearInput()
    showStoredValue()
    showInfo("Storage cleared")
}

// init components (called after page is loaded)
function initComponents() {
    storeButton = document.getElementById("btn_store")
    clearButton = document.getElementById("btn_clear")
    numberField = document.getElementById("tf_numberinput")
    storedValueOutput = document.getElementById("out_currentvalue")
    errorBlock = document.getElementById("error-not-a-number")
    infoBlock = document.getElementById("info")

    storeButton.onclick = function() { storeValue() }
    clearButton.onclick = function() { clear() }
    showStoredValue()

    dbg("components initalized")
}
