# Local storage experiment

A simple form with an input field and two buttons:

* one button stores the NUMBER value of input into localStorage
* another button removes any stored value

An existing value is overridden.

If input not a number, an error is displayed till next input.
